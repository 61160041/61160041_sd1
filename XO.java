import java.util.Scanner;

public class XO {
	public static Scanner kb = new Scanner(System.in);
	public static char[][] Table = new char[3][3];
	public static char turn = 'O';
	public static Boolean check = false;
	public static int count = 0;

	static char creatTable() {
		System.out.println("Welcome to XO Game");
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				Table[row][col] = '-';
			}
		}
		return 0;
	}

	static char showTable() {
		for (int row = 0; row < 3; row++) {
			System.out.println(Table[row][0] + " " + Table[row][1] + " " + Table[row][2]);
		}
		return 0;
	}

	static char chanceturn() {
		if (turn == 'O') {
			turn = 'X';
		} else
			turn = 'O';
		return 0;
	}

	static int input() {
		System.out.println("Turn " + turn);
		System.out.println("Please input row and collum : ");
		for (;;) {
			int Row = kb.nextInt();
			int Col = kb.nextInt();
			if (Row < 1 || Row > 3 || Col < 1 || Col > 3) {
				System.out.println("Please input row and collum again : ");
			} else if (Table[Row - 1][Col - 1] == '-' & Table[Row - 1][Col - 1] != turn) {
				Table[Row - 1][Col - 1] = turn;
				break;
			} else
				System.out.println("Please input row and collum again : ");
		}
		return 0;
	}

	static boolean check() {
		for (int row = 0; row < 3; row++) {
			if (Table[row][0] == turn & Table[row][1] == turn & Table[row][2] == turn) {
				showTable();
				System.out.println("Player " + turn + " win");
				check = true;
				break;
			}
			if (Table[0][0] == turn & Table[1][1] == turn & Table[2][2] == turn
					| Table[0][2] == turn & Table[1][1] == turn & Table[2][0] == turn) {
				showTable();
				System.out.println("Player " + turn + " win");
				check = true;
				break;
			}
			if (Table[0][row] == turn & Table[1][row] == turn & Table[2][row] == turn) {
				showTable();
				System.out.println("Player " + turn + " win");
				check = true;
				break;
			}
		}
		return false;
	}

	public static void main(String[] args) {
		creatTable();
		while (true) {
			showTable();
			input();
			check();
			if (check == true) {
				break;
			} else if (count == 8) {
				showTable();
				System.out.println("Draw");
				break;
			}
			chanceturn();
			count++;
		}
	}
}

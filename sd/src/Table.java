
public class Table {
	static char[][] table = new char[3][3];
	
	static char creatTable() {
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				table[row][col] = '-';
			}
		}
		return 0;
	}

	static char showTable() {
		for (int row = 0; row < 3; row++) {
			System.out.println(table[row][0] + " " + table[row][1] + " " + table[row][2]);
		}
		return 0;
	}
}
